# 原神空荧地图后端组件-Websocket消息实时分发器

## 部署参数

```
-DSERVER_PORT=启动端口
-DRABBITMQ_HOST=消息队列服务器地址
-DRABBITMQ_PORT=消息队列服务器端口
-DRABBITMQ_USER=消息队列服务器用户名
-DRABBITMQ_PWD=消息队列服务器密码
```

## 默认启动指令

`java -DSERVER_PORT=8080 -DRABBITMQ_HOST=localhost -DRABBITMQ_PORT=5672 -DRABBITMQ_USER=guest -DRABBITMQ_PWD=guest -jar .\genshin-map-netty-distribute-0.0.1-SNAPSHOT.jar`

## 连接接口

`ws://url:port/local/ws?username=用户名`

### Header

| key           | value      | 描述         |
|---------------|------------|------------|
| authorization | Bearer xxx | Bearer验证密钥 |

### Params

| key      | value | 描述  |
|----------|-------|-----|
| username | xxx   | 用户名 |

## TODO

- [ ] 完善注释文档
- [ ] 完善消息队列服务器配置
- [ ] 实现分发器集群