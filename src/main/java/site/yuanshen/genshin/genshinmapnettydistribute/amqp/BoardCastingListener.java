package site.yuanshen.genshin.genshinmapnettydistribute.amqp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import site.yuanshen.genshin.genshinmapnettydistribute.websocket.WebSocketWrap;

/**
 * TODO
 *
 * @author Moment
 */
@Component
@Slf4j
@RabbitListener(queues = "broadcast")
public class BoardCastingListener {

    @RabbitHandler
    public void process(String message) {
        log.info("Receive message, broadcasting: {}", message);
        WebSocketWrap.broadcastText(message);
    }

}
