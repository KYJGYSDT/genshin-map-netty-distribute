package site.yuanshen.genshin.genshinmapnettydistribute.amqp;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * TODO
 *
 * @author Moment
 */
@Configuration
public class RabbitConfiguration {

    @Bean
    public Queue boardCastQueue() {
        return new Queue("broadcast", false);
    }

}
