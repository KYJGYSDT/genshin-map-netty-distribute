package site.yuanshen.genshin.genshinmapnettydistribute.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/**
 * TODO
 *
 * @author Moment
 */
@Component
@Slf4j
public class SecurityPrincipleFilter implements WebFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        return ReactiveSecurityContextHolder.getContext().flatMap(securityContext -> {
            if (securityContext.getAuthentication() != null) {
                BearerTokenAuthentication authentication = (BearerTokenAuthentication) securityContext.getAuthentication();
                ServerHttpRequest request = exchange.getRequest().mutate()
                        .headers(httpHeaders -> httpHeaders.remove("Authorization"))
                        .header("user_name", authentication.getTokenAttributes().get("user_name").toString()).build();
                return chain.filter(exchange.mutate().request(request).build());
            }
            return chain.filter(exchange);
        });
    }
}
