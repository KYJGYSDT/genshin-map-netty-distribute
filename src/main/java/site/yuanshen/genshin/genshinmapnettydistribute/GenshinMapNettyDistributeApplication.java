package site.yuanshen.genshin.genshinmapnettydistribute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenshinMapNettyDistributeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenshinMapNettyDistributeApplication.class, args);
    }

}
