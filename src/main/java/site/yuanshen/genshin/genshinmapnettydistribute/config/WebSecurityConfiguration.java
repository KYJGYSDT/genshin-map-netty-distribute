package site.yuanshen.genshin.genshinmapnettydistribute.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import site.yuanshen.genshin.genshinmapnettydistribute.filter.SecurityPrincipleFilter;

/**
 * TODO
 *
 * @author Moment
 */
@Configuration
public class WebSecurityConfiguration {

    @Bean
    public SecurityWebFilterChain securityFilterChain(ServerHttpSecurity http) {
        http.csrf().disable();
        http.authorizeExchange()
                .pathMatchers("/oauth/**","/test/**").permitAll()
                .anyExchange().authenticated();
        http.addFilterAfter(new SecurityPrincipleFilter(), SecurityWebFiltersOrder.AUTHENTICATION);
        http.oauth2ResourceServer().opaqueToken();
        return http.build();
    }
}
