package site.yuanshen.genshin.genshinmapnettydistribute.websocket;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.FluxSink;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO
 *
 * @author Moment
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class WebSocketWrap {
    public static final Map<String, WebSocketWrap> SENDER = new ConcurrentHashMap<>();

    private String username;
    private WebSocketSession session;
    private FluxSink<WebSocketMessage> sink;

    /**
     * 发送广播消息
     *
     * @param obj 消息对象，会被转为JSON
     */
    public static void broadcastText(Object obj) {
        SENDER.values().forEach(wrap -> wrap.sendText(obj));
    }

    /**
     * 通过用户列表向指定用户发送消息
     * @param users 用户名列表
     * @param obj  消息对象，会被转为JSON
     */
    public static void sendToUsers(List<String> users, Object obj) {
        SENDER.values().forEach(wrap -> {
            if(users.contains(wrap.getUsername())) {
                wrap.sendText(obj);
            }
        });
    }

    public void sendText(Object obj) {
        sink.next(session.textMessage(JSON.toJSONString(obj)));
    }

}
