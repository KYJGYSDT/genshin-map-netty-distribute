package site.yuanshen.genshin.genshinmapnettydistribute.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.HandshakeInfo;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * TODO
 *
 * @author Moment
 */
@Slf4j
@Component
public class DistributeWebSocketHandler implements WebSocketHandler {

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        HandshakeInfo handshakeInfo = session.getHandshakeInfo();
        String username =getQueryMap(handshakeInfo.getUri().getQuery()).get("username");
        if(!Objects.requireNonNull(
                handshakeInfo.getHeaders().get("user_name")).get(0).equals(username)){
            return session.send(Mono.just(session.textMessage("请求非法")));
        }
        log.info("username:{}",username);
        Mono<Void> input = session.receive().doOnNext(message -> this.messageHandle(session, message))
                .log()
                .doOnError(throwable -> log.error("webSocket发生异常：" + throwable))
                .doOnComplete(() -> {
                    log.info("webSocket结束");
                    WebSocketWrap.SENDER.remove(username);
                }).then();
        Mono<Void> output = session.send(Flux.create(
                sink -> WebSocketWrap.SENDER.put(username,
                        new WebSocketWrap(username, session, sink)))
        );
        return Mono.zip(input, output).then();
    }

    private void messageHandle(WebSocketSession session, WebSocketMessage message) {
        // 接收客户端请求的处理回调
        switch (message.getType()) {
            case TEXT:
            case BINARY:
            case PONG:
            case PING:
                break;
            default:
        }
    }

    private Map<String, String> getQueryMap(String queryStr) {
        Map<String, String> queryMap = new HashMap<>(4);
        if (!queryStr.isEmpty()) {
            String[] queryParam = queryStr.split("&");
            Arrays.stream(queryParam).forEach(s -> {
                String[] kv = s.split("=", 2);
                String value = kv.length == 2 ? kv[1] : "";
                queryMap.put(kv[0], value);
            });
        }
        return queryMap;
    }
}
